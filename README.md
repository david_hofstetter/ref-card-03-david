# Ref Card 03
Das Projekt Ref-Card-03 ist eine Springboot-Application, die mit einer Datenbank zusammenarbeitet. Darauf kann man verschiedene Witze anschauen.

## Requirements
- Du hast ein Windows/Linux/Mac Betriebssystem
- Es braucht mindestens die Openjdk-Version 11

## Installation and Usage
### Maven
- mvn package
- mvn clean
- mvn site
### Docker
- docker build .
- docker tag [image id] [Repository Name]:[Tag]
- docker run -d -p 8080:8080 [Repository Name]:[Tag]